// $Id: expr-recdes.cpp,v 1.9 2016-04-01 16:43:06-07 - - $

//
// A trivial hand-coded top-down recursive descent compiler for
// a simple language.  No leak checking.  Just crash on any
// syntax error.
//
// Context-free syntax.
// ->, {, }, |, ... are metasymbols
//
// program -> { expr ; }...
// expr    -> term { { + | - } term }...
// term    -> factor { { * | / } factor }...
// factor  -> ( expr ) | IDENT | NUMBER
//
// Lexical syntax.
// Using flex notation.
//
// IDENT   -> [A-Za-z][A-Za-z0-9]*
// NUMBER  -> [0-9]+
// COMMENT -> #.*
// WHITE   -> [ \n]+
//

#include <cassert>
#include <cctype>
#include <cstdlib>
#include <ios>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;

enum { ENDFILE = 256, IDENT = 257, NUMBER = 258,
       ROOT = 259, NOSYMBOL = 260 };

unordered_map<unsigned,string> symbol_names {
   {ENDFILE,  "ENDFILE"},
   {IDENT,    "IDENT"},
   {NUMBER,   "NUMBER"},
   {ROOT,     "ROOT"},
   {NOSYMBOL, "NOSYMBOL"},
};

void print_symbol (unsigned sym) {
   cout << sym;
   const auto& isym = symbol_names.find (sym);
   if (isym != symbol_names.cend()) {
      cout << "(" << isym->second << ")";
   }else if (isgraph (sym)) {
      cout << "('" << static_cast<char> (sym) << "')";
   }
}


struct astree {
   unsigned symbol {};
   string lexeme;
   vector<astree*> children;
   void adopt (astree* child) { children.push_back (child); }
   astree (int sym, const string& lex): symbol(sym), lexeme(lex) {}
   ~astree() { for (auto child: children) delete child; }
};

void preorder_astree (size_t depth, const astree* tree) {
   for (size_t count = 0; count < depth; ++count) cout << "|  ";
   cout << "\"" << tree->lexeme << "\" ";
   print_symbol (tree->symbol);
   cout << endl;
   for (const astree* child: tree->children) {
      preorder_astree (depth + 1, child);
   }
}

void print_astree (const char* func, int line, const astree* tree) {
   cout << func << "[" << line << "]:" << endl;
   preorder_astree (1, tree);
}

#define PRINT_ASTREE(TREE) print_astree (__func__, __LINE__, TREE)


class scanner {
   private:
      int peekchar {getchar()};
      astree* lookahead {nullptr};
      string scan_chars (int (*) (int));
      void advance (int symbol, int (*ischar) (int));
   public:
      scanner() { get_token(); }
      ~scanner() { delete lookahead; }
      unsigned peek_symbol() const { return lookahead->symbol; }
      astree* get_token();
      void skip_token() { delete get_token(); }
};

string scanner::scan_chars (int (*ischar) (int)) {
   string lexeme;
   do {
      assert (peekchar != EOF);
      lexeme += peekchar;
      peekchar = getchar();
   }while (ischar and ischar (peekchar));
   return lexeme;
}

void scanner::advance (int symbol, int (*ischar) (int)) {
   string lexeme = symbol != ENDFILE ? scan_chars (ischar) : "<<EOF>>";
   lookahead = new astree (symbol, lexeme);
}


astree* scanner::get_token() {
   astree* token = lookahead;
   for (;;) {
      if (isalpha (peekchar)) {
         advance (IDENT, isalnum);
         break;
      }
      if (isdigit (peekchar)) {
         advance (NUMBER, isdigit);
         break;
      }
      switch (peekchar) {
         case ' ':
         case '\n':
            scan_chars (isspace);
            continue;
         case '#':
            scan_chars ([] (int chr) -> int {
                           return chr != '\n' and chr != EOF;
                        });
            continue;
         case '+':
         case '-':
         case '*':
         case '/':
         case '(':
         case ')':
         case ';':
            advance (peekchar, nullptr);
            break;
         case EOF:
            advance (ENDFILE, nullptr);
            break;
         default:
            assert (false);
      }
      break;
   }
   if (token != nullptr) PRINT_ASTREE (token);
   return token;
}


astree* parse_expr (scanner& scanner);

astree* parse_factor (scanner& scanner) {
   astree* tree = nullptr;
   switch (scanner.peek_symbol()) {
      case '(':
         scanner.skip_token();
         tree = parse_expr (scanner);
         assert (scanner.peek_symbol() == ')');
         scanner.skip_token();
         break;
      case IDENT:
      case NUMBER:
         tree = scanner.get_token();
         break;
      default:
         assert (scanner.peek_symbol() == NOSYMBOL);
   }
   PRINT_ASTREE (tree);
   return tree;
}

astree* parse_term (scanner& scanner) {
   astree* root = parse_factor (scanner);
   for (;;) {
      switch (scanner.peek_symbol()) {
         case '*':
         case '/': {
            astree* oper = scanner.get_token();
            oper->adopt (root);
            oper->adopt (parse_factor (scanner));
            root = oper;
            break;
         }
         default:
            PRINT_ASTREE (root);
            return root;
      }
   }
}


astree* parse_expr (scanner& scanner) {
   astree* root = parse_term (scanner);
   for (;;) {
      switch (scanner.peek_symbol()) {
         case '+':
         case '-': {
            astree* oper = scanner.get_token();
            oper->adopt (root);
            oper->adopt (parse_term (scanner));
            root = oper;
            break;
         }
         default:
            PRINT_ASTREE (root);
            return root;
      }
   }
}

void parse_program (scanner& scanner) {
   astree* root = new astree (ROOT, "<<ROOT>>");
   while (scanner.peek_symbol() != ENDFILE) {
      astree* tree = parse_expr (scanner);
      cout << "\f" << endl;
      assert (scanner.peek_symbol() == ';');
      scanner.skip_token();
      root->adopt (tree);
   }
   PRINT_ASTREE (root);
   delete root;
}

int main (void) {
   scanner scanner;
   parse_program (scanner);
   assert (scanner.peek_symbol() == ENDFILE);
   return EXIT_SUCCESS;
}

