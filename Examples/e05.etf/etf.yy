%{
// $Id: etf.yy,v 1.33 2016-10-28 14:23:35-07 - - $

#include <cassert>
#include <cctype>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#include <unistd.h>

struct tree {
   static int next_node_id;
   int node_id {++next_node_id};;
   int Vt;
   char lexeme;
   vector<tree*> children;
   tree (int Vt, char lexeme): Vt(Vt), lexeme(lexeme) {}
   ~tree();
   tree* adopt (tree*, tree* = nullptr);
   void preorder (int depth);
};
int tree::next_node_id {0};

string to_string (tree*);
void show (tree*, bool want_print = false);
void destroy (tree* = nullptr, tree* = nullptr);

#define YYSTYPE_IS_DECLARED
using YYSTYPE = tree*;
int yylex();
void yyerror (tree*&, const char*);

%}


%debug
%defines
%error-verbose
%token-table
%verbose

%destructor { delete $$; } <>
%printer { if (yydebug) cerr << to_string ($$); } <>
%parse-param {tree*& root}

%token IDENT ADD MUL LPAR RPAR
%start start

%%

start  : expr                { root = $1; $$ = nullptr; show (root); }
       ;

expr   : expr ADD term       { $$ = $2->adopt ($1, $3); show ($$); }
       | term                { $$ = $1; show ($$); }
       ;

term   : term MUL factor     { $$ = $2->adopt ($1, $3); show ($$); }
       | factor              { $$ = $1; show ($$); }
       ;

factor : LPAR expr RPAR      { destroy ($1, $3); $$ = $2; show ($$); }
       | IDENT               { $$ = $1; show ($$); }
       ;

%%


tree::~tree() {
   while (not children.empty()) {
      delete children.back();
      children.pop_back();
   }
   cerr << "Deleting " << to_string (this) << endl;
}

void destroy (tree* node1, tree* node2) {
   if (node1 != nullptr) delete node1;
   if (node2 != nullptr) delete node2;
}

tree* tree::adopt (tree* child1, tree* child2) {
   if (child1 != nullptr) children.push_back (child1);
   if (child2 != nullptr) children.push_back (child2);
   return this;
}

void tree::preorder (int depth) {
   cerr << "AST: " << string (depth * 3, ' ') << to_string (this)
        << endl;
   for (auto node: children) node->preorder (depth + 1);
}

void show (tree* node, bool want_print) {
   if (want_print or yydebug) {
      if (node == nullptr) cerr << "AST: nullptr" << endl;
                      else node->preorder (0);
   }
}

string visible (char ch) {
   char buffer[16];
   snprintf (buffer, sizeof buffer,
             isgraph (ch) ? "'%c'" : "'\\x%02X'", ch);
   return buffer;
}

string to_string (tree* node) {
   stringstream result;
   if (node == nullptr) result << "nullptr";
   else {
      result << "node" << node->node_id << ": "
             << yytname[YYTRANSLATE(node->Vt)] << " "
             << visible (node->lexeme) << " [";
      string space = "";
      for (auto item: node->children) {
         result << space << "->node" << item->node_id;
         space = " ";
      }
      result << "]";
   }
   return result.str();
}


char *input_string = nullptr;
char *nextchar = nullptr;
int exit_status = EXIT_SUCCESS;

int set_yylval (int Vt, char lexeme) {
   yylval = new tree (Vt, lexeme);
   if (yydebug) cerr << "yylex: returns (" << to_string (yylval)
                     << ")" << endl;
   return Vt;
}

int yylex (void) {
   for (;;) {
      int ch = *nextchar++;
      if (ch == '\0') return set_yylval (0, ch);
      if (isspace (ch)) continue;
      if (isalnum (ch)) return set_yylval (IDENT, ch);
      switch (ch) {
         case '+': return set_yylval (ADD, ch);
         case '*': return set_yylval (MUL, ch);
         case '(': return set_yylval (LPAR, ch);
         case ')': return set_yylval (RPAR, ch);
      }
      cerr << "Bad character " << visible (ch) << endl;
      exit_status = EXIT_FAILURE;
   }
}

void yyerror (tree*&, const char* message) {
   cerr << message << endl;
   exit_status = EXIT_FAILURE;
}

int main (int argc, char** argv) {
   yydebug = 0;
   for (;;) {
      int opt = getopt (argc, argv, "y");
      if (opt == EOF) break;
      if (opt == 'y') yydebug = 1;
   }
   assert (optind == argc - 1);
   ios_base::sync_with_stdio (true);
   nextchar = input_string = argv[optind];
   cerr << "Input: \"" << input_string << "\"" << endl;
   tree* root = nullptr;
   int parse_status = yyparse (root);
   cerr << "Parse status " << parse_status << endl;
   if (yydebug) cerr << "Input: \"" << input_string << "\"" << endl;
   cerr << "Root of AST:" << endl;
   show (root, true);
   destroy (root);
   if (parse_status == 0) destroy (yylval);
   return exit_status | parse_status;
}

