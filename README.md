# CS 104A #

## DRAFT OF NARRATIVE EVALUATION
Of the five programming projects, which together constituted
a compiler for a small C-like language: Part-1, the stringset
manager and popen to the C preprocessor, was most excellent
(A++).  Part-2, the lexical analyzer, written using flex,
was most excellent (A++).  Part-3, the parser, written using
bison, was most excellent (A++).  Part-4, the symbol table
manager and type checker was most excellent (A++).  Part-5, the
intermediate language code generator was most excellent (A++).