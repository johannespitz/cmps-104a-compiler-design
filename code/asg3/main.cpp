// $Id: main.cpp,v 1.3 2017-04-09 19:03:16-07 - - $

/*
This program was completed using pair programming.
Partner:  Bradley Bernard (bmbernar@ucsc.edu)
Partner:  Johannes Pitz (jpitz@ucsc.edu)
*/

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "astree.h"
#include "auxlib.h"
#include "lyutils.h"
#include "string_set.h"

const string cpp_name = "/usr/bin/cpp";
string cpp_command;
string cpp_flags;
constexpr size_t LINESIZE = 1024;

// Chomp the last character from a buffer if it is delim.
void chomp (char* string, char delim) {
   size_t len = strlen (string);
   if (len == 0) return;
   char* nlpos = string + len - 1;
   if (*nlpos == delim) *nlpos = '\0';
}

// Open a pipe from the C preprocessor.
// Exit failure if can't.
// Assigns opened pipe to FILE* yyin.
void cpp_popen (const char* filename) {
   cpp_command = cpp_name + " " + cpp_flags + " " + filename;
   yyin = popen (cpp_command.c_str(), "r");
   if (yyin == NULL) {
      syserrprintf (cpp_command.c_str());
      exit (exec::exit_status);
   } else {
      if (yy_flex_debug) {
         fprintf (stderr, "-- popen (%s), fileno(yyin) = %d\n",
                  cpp_command.c_str(), fileno (yyin));
      }
   }
}

// Close pipe yyin. 
// If error set exit_status to failure.
void cpp_pclose() {
   int pclose_rc = pclose (yyin);
   eprint_status (cpp_command.c_str(), pclose_rc);
   if (pclose_rc != 0) exec::exit_status = EXIT_FAILURE;
}

// Scan options in argv to setup debug flags
// and cpp flags.
void scan_opts (int argc, char** argv) {
   opterr = 0;
   yy_flex_debug = 0;
   yydebug = 0;
   cpp_flags = "";
   lexer::interactive = isatty (fileno (stdin))
                    and isatty (fileno (stdout));
   for(;;) {
      int opt = getopt (argc, argv, "lyD:@:");
      if (opt == EOF) break;
      switch (opt) {
         case '@': set_debugflags(optarg); break;
         case 'l': yy_flex_debug = 1;         break;
         case 'y': yydebug = 1;               break;
         case 'D': cpp_flags = string("-D") + string(optarg); break; 
         default:  errprintf ("Bad option (%c)\n", optopt); break;
      }
   }
   if (optind > argc) {
      errprintf ("Usage: %s [-ly] [-@flag...] [-Dstring] program.oc\n",
                 exec::execname.c_str());
      exit (exec::exit_status);
   }
}

int main (int argc, char** argv) {
   exec::execname = basename (argv[0]);
   scan_opts (argc, argv);

   if (yydebug or yy_flex_debug) {
      fprintf (stderr, "Command:");
      for (char** arg = &argv[0]; arg < &argv[argc]; ++arg) {
            fprintf (stderr, " %s", *arg);
      }
      fprintf (stderr, "\n");
   }
   
   const char* filename = optind == argc ? "-" : argv[optind];
   string outfile = string(basename(filename));

   if (outfile.substr(outfile.find_last_of('.'), string::npos) 
            != ".oc") {
      errprintf ("Usage: %s [-ly] [-@flag...] [-Dstring] program.oc\n",
                 exec::execname.c_str());
      exit (exec::exit_status);
   }

   outfile = outfile.substr(0, outfile.find_last_of('.')) + ".str";   
   string tokens;
   tokens = outfile.substr(0, outfile.find_last_of('.')) + ".tok";
   string tree;
   tree = outfile.substr(0, outfile.find_last_of('.')) + ".ast";

   cpp_popen (filename);
   DEBUGF('a', "%s", outfile.c_str());
   DEBUGF('a', "%s", tokens.c_str());
   DEBUGF('a', "%s", tree.c_str());
   
   tokFs = fopen(tokens.c_str(), "w");
   int yy_rc = yyparse();

   fclose(tokFs);  
   cpp_pclose();
   yylex_destroy();

   FILE* file = fopen(outfile.c_str(), "w");
   if(file == NULL) {
      syserrprintf ("fopen");
      exit (exec::exit_status);
   }
   string_set::dump(file);
   fclose(file);

   if (yydebug or yy_flex_debug) {
      fprintf (stderr, "Dumping parser::root:\n");
      if (parser::root != nullptr) parser::root->dump_tree (stderr);
      fprintf (stderr, "Dumping string_set:\n");
      string_set::dump (stderr);
   }

   if(yy_rc) {
      errprintf ("parse failed (%d)\n", yy_rc);
      exit (exec::exit_status);
   }

   file = fopen(tree.c_str(), "w");
   if(file == NULL) {
      syserrprintf ("fopen");
      exit (exec::exit_status);
   }

   astree::print(file, parser::root);
   delete parser::root;
   
   return exec::exit_status;
}
