%{

#include <cassert>

#include "lyutils.h"
#include "astree.h"

%}

%debug
%defines
%error-verbose
%token-table
%verbose

%destructor { destroy ($$); } <>
%printer { astree::dump (yyoutput, $$); } <>

%initial-action {
   parser::root = new astree (ROOT, {0, 0, 0}, "<<ROOT>>");
}

%token  ROOT

%token TOK_VOID TOK_CHAR TOK_INT TOK_STRING
%token TOK_IF TOK_ELSE TOK_WHILE TOK_RETURN TOK_STRUCT
%token TOK_NULL TOK_NEW TOK_ARRAY
%token TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%token TOK_IDENT TOK_INTCON TOK_CHARCON TOK_STRINGCON 
%token TOK_DIRECTIVE 

%token TOK_BLOCK TOK_CALL TOK_IFELSE TOK_INITDECL
%token TOK_POS TOK_NEG TOK_NEWARRAY TOK_FIELD
%token TOK_NEWSTRING TOK_TYPEID TOK_VARDECL TOK_DECLID 
%token TOK_RETURNVOID TOK_INDEX TOK_FUNCTION 
%token TOK_PROTOTYPE TOK_PARAMLIST
%token TOK_ORD TOK_CHR TOK_ROOT

// %left   '{'
%right  TOK_IF TOK_ELSE
%right  '='
%left   TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%left   '.' '['
%left   '+' '-'
%left   '*' '/' '%'
%right  TOK_POS TOK_NEG '!' TOK_NEW 

%start root

%%

root          : program                                   
                { $$ = $1 = nullptr; }
              ;

program       : program structdef                         
                { $$ = $1->adopt($2);  }
              | program function                          
                { $$ = $1->adopt($2);  }
              | program stmt                              
                { $$ = $1->adopt ($2); }
              | program error '}'                         
                { $$ = $1; destroy($3); }
              | program error ';'                         
                { $$ = $1; destroy($3); }
              |                                           
                { $$ = parser::root; }
              ;

block         : stmtseq '}'                               
                { $$ = $1; destroy($2); }
              | '{' '}'                                   
                { $1->substitute(TOK_BLOCK); $$ = $1; destroy($2); }
              ;

stmtseq       : stmtseq stmt                              
                { $$ = $1->adopt($2); }
              | '{' stmt                                  
                { $1->substitute(TOK_BLOCK); $$ = $1->adopt($2); }
              ;

stmt          : block                                     
                { $$ = $1; }
              | vardecl                                   
                { $$ = $1; }
              | while                                     
                { $$ = $1; }
              | ifelse                                    
                { $$ = $1; } 
              | return                                    
                { $$ = $1; }
              | expr ';'                                  
                { $$ = $1; destroy($2); }
              | ';'                                       
                { $$ = $1; }
              ;

vardecl       : identdecl '=' expr ';'                    
                { 
                   $2->substitute(TOK_VARDECL); 
                   $$ = $2->adopt($1, $3); 
                   destroy($4); 
                }
              ;

while         : TOK_WHILE '(' expr ')' stmt               
                { $$ = $1->adopt($3, $5); destroy($2, $4); }
              ;

ifelse        : TOK_IF '(' expr ')' stmt %prec TOK_ELSE   
                { $$ = $1->adopt($3, $5); destroy($2, $4); }
              | TOK_IF '(' expr ')' stmt TOK_ELSE stmt    
                { $$ = $1->adopt($3, $5, $7); destroy($2, $4, $6); }
              ;

return        : TOK_RETURN ';'                            
                { $1->substitute(TOK_RETURNVOID); $$ = $1; destroy($2);}
              | TOK_RETURN expr ';'                       
                { $$ = $1->adopt($2); destroy($3); }
              ;

expr          : expr TOK_EQ expr                          
                { $$ = $2->adopt ($1, $3); }
              | expr TOK_GT expr                          
                { $$ = $2->adopt($1, $3); }
              | expr TOK_GE expr                          
                { $$ = $2->adopt($1, $3); }
              | expr TOK_LT expr                          
                { $$ = $2->adopt($1, $3); }
              | expr TOK_LE expr                          
                { $$ = $2->adopt($1, $3); }
              | expr TOK_NE expr                          
                { $$ = $2->adopt($1, $3); }
              | expr '+' expr                             
                { $$ = $2->adopt ($1, $3); }
              | expr '=' expr                             
                { $$ = $2->adopt ($1, $3); }
              | expr '-' expr                             
                { $$ = $2->adopt ($1, $3); }
              | expr '*' expr                             
                { $$ = $2->adopt ($1, $3); }
              | expr '/' expr                             
                { $$ = $2->adopt ($1, $3); }
              | expr '%' expr                             
                { $$ = $2->adopt($1, $3); }
              | '!' expr %prec '!'                        
                { $$ = $1->adopt_sym ($2, '!'); }
              | '+' expr %prec TOK_POS                    
                { $$ = $1->adopt_sym ($2, TOK_POS); }
              | '-' expr %prec TOK_NEG                    
                { $$ = $1->adopt_sym ($2, TOK_NEG); }
              | '(' expr ')'                              
                { destroy ($1, $3); $$ = $2; }
              | allocator                                 
                { $$ = $1; }
              | call                                      
                { $$ = $1; }
              | variable                                  
                { $$ = $1; }
              | constant                                  
                { $$ = $1; }
              ;

call          : TOK_IDENT '(' ')'                         
                { 
                  $2->substitute(TOK_CALL); 
                  $$ = $2->adopt($1); 
                  destroy($3); 
                }
              | TOK_IDENT '(' arguments ')'               
                { 
                   $2->substitute(TOK_CALL); 
                   $2->adopt($1); 
                   $$ = $2->adopt_children($3); 
                   free($3); destroy($4); 
                }
              ;


// @ symbol for the new astree*
// node as a dummy token. deleting later

arguments     : arguments ',' expr                        
                { $$ = $1->adopt($3); destroy($2); }
              | expr                                      
                { astree* node = $1->change('@'); $$ = node->adopt($1);}
              ;


allocator     : TOK_NEW TOK_IDENT '(' ')'                 
                { 
                   $2->substitute(TOK_TYPEID); 
                   $$ = $1->adopt($2); 
                   destroy($3, $4); 
                }
              | TOK_NEW TOK_STRING '(' expr ')'           
                { 
                   $1->substitute(TOK_NEWSTRING); 
                   $$ = $1->adopt($4); 
                   destroy($2, $3, $5); 
                }
              | TOK_NEW basetype '[' expr ']'             
                { 
                   $1->substitute(TOK_NEWARRAY); 
                   $$ = $1->adopt($2, $4); 
                   destroy($3, $5); 
                }
              ;

variable      : TOK_IDENT                                 
                { $$ = $1; }
              | expr '[' expr ']'                         
                { 
                   $2->substitute(TOK_INDEX); 
                   $$ = $2->adopt($1, $3); 
                   destroy($4); 
                }
              | expr '.' TOK_IDENT                        
                { $3->substitute(TOK_FIELD); $$ = $2->adopt($1, $3); }
              ;

constant      : TOK_INTCON                                
                { $$ = $1; }
              | TOK_CHARCON                               
                { $$ = $1; }
              | TOK_STRINGCON                             
                { $$ = $1; }
              | TOK_NULL                                  
                { $$ = $1; }
              ;

function      : identdecl identdecls ')' block            
                { 
                   astree* par = $1->change(TOK_FUNCTION); 
                   $$ = par->adopt($1, $2, $4); 
                   destroy($3); 
                }
              | identdecl identdecls ')' ';'              
                { 
                   astree* par = $1->change(TOK_PROTOTYPE); 
                   $$ = par->adopt($1, $2); 
                   destroy($3, $4); 
                }
              | identdecl '(' ')' block                   
                { 
                   astree* par = $1->change(TOK_FUNCTION); 
                   $2->substitute(TOK_PARAMLIST); 
                   $$ = par->adopt($1, $2, $4); 
                   destroy($3); 
                }
              | identdecl '(' ')' ';'                     
                { 
                   astree* par = $1->change(TOK_PROTOTYPE); 
                   $2->substitute(TOK_PARAMLIST); 
                   $$ = par->adopt($1, $2); 
                   destroy($3, $4); 
                }
              ;


identdecls    : identdecls ',' identdecl                  
                { $$ = $1->adopt($3); destroy($2); }
              | '(' identdecl                             
                { $1->substitute(TOK_PARAMLIST); $$ = $1->adopt($2); }
              ;

identdecl     : basetype TOK_IDENT                        
                { $2->substitute(TOK_DECLID); $$ = $1->adopt($2); }
              | basetype TOK_ARRAY TOK_IDENT              
                { $3->substitute(TOK_DECLID); $$ = $2->adopt($1, $3); }
              ;


structdef     : TOK_STRUCT TOK_IDENT fielddecls '}'       
                { 
                   $2->substitute(TOK_TYPEID); 
                   $1->adopt($2); 
                   $$ = $1->adopt_children($3); 
                   destroy($4); 
                }
              | TOK_STRUCT TOK_IDENT '{' '}'              
                { 
                   $2->substitute(TOK_TYPEID); 
                   $$ = $1->adopt($2); 
                   destroy($3, $4); 
                }
              ;

fielddecls    : fielddecls fielddecl                      
                { $$ = $1->adopt($2); }
              | '{' fielddecl                             
                { $$ = $1->adopt($2); }
              ;

fielddecl     : basetype TOK_IDENT ';'                    
                { 
                   $2->substitute(TOK_FIELD); 
                   $$ = $1->adopt($2); 
                   destroy($3); 
                }
              | basetype TOK_ARRAY TOK_IDENT ';'          
                { 
                   $3->substitute(TOK_FIELD); 
                   $$ = $2->adopt($1, $3); 
                   destroy($4); 
                }
              ;

basetype      : TOK_VOID                                  
                { $$ = $1; }
              | TOK_INT                                   
                { $$ = $1; }
              | TOK_STRING                                
                { $$ = $1; }
              | TOK_IDENT                                 
                { $1->substitute(TOK_TYPEID); $$ = $1; }
              ;

%%

const char *parser::get_tname (int symbol) {
   return yytname [YYTRANSLATE (symbol)];
}

bool is_defined_token (int symbol) {
   return YYTRANSLATE (symbol) > YYUNDEFTOK;
}
