// $Id: string_set.h,v 1.2 2017-04-15 16:21:52-07 - - $

/*
This program was completed using pair programming.
Partner:  Bradley Bernard (bmbernar@ucsc.edu)
Partner:  Johannes Pitz (jpitz@ucsc.edu)
*/

#ifndef __STRING_SET__
#define __STRING_SET__

#include <string>
#include <unordered_set>
using namespace std;

#include <stdio.h>

struct string_set {
   string_set();
   static unordered_set<string> set;
   static const string* intern (const char*);
   static void dump (FILE*);
};

#endif

