// $Id: astree.h,v 1.1 2017-04-09 17:04:29-07 - - $

#ifndef __ASTREE_H__
#define __ASTREE_H__

#include <string>
#include <vector>
using namespace std;

struct location {
   size_t filenr;
   size_t linenr;
   size_t offset;
};

struct astree;

#include "auxlib.h"
#include "symbol.h"


struct astree {

   // Fields.
   int symbol;               // token code
   location lloc;            // source location
   const string* lexinfo;    // pointer to lexical information
   vector<astree*> children; // children of this n-way node

   // ASG4
   attr_bitset attributes;
   size_t blocknr;
   const string* type_id;
   symbol_t* sym_node;
   
   // ASG5
   string vreg;

   // Functions.
   astree (int symbol, const location&, const char* lexinfo);
   ~astree();

   astree* adopt (astree* child1,
                  astree* child2 = nullptr,
                  astree* child3 = nullptr);
   astree* adopt_sym (astree* child, int symbol);
   astree* adopt_children (astree* child);
   astree* change (int symbol_);
   void substitute(int symbol);
   void dump_node (FILE*);
   void dump_tree (FILE*, int depth = 0);
   static void dump (FILE* outfile, astree* tree);
   static void print (FILE* outfile, astree* tree, int depth = 0);
   static void print_sym (FILE* outfile, astree* sym);
};

void destroy (astree* tree1,
              astree* tree2 = nullptr,
              astree* tree3 = nullptr);

void errllocprintf (const location&, const char* format, const char*);

#endif
