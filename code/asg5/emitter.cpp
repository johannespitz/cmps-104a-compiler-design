// $Id: symbol.cpp,v 1.1 2017-04-09 17:04:29-07 - - $

#include "astree.h"
#include "lyutils.h"
#include "symbol.h"
#include "string_set.h"
#include "emitter.h"



FILE* oilFile;
extern vector<astree*> stringConsVector;
extern symbol_table* type_names;
extern vector<symbol_table*> symbol_stack;


string emit (astree* node);
void print_stmt (astree* node);

int i, a, p;

string get_type (symbol* sym) {
   if (sym == nullptr) 
      return "Error: type of non symbol";
   string array = "";
   if (sym->attributes[ATTR_array])
      array = "*";
   if (sym->attributes[ATTR_string])
      return "char*" + array;
   if (sym->attributes[ATTR_int])
      return "int" + array;
   if (sym->attributes[ATTR_struct])
      return "struct s_" + *sym->type_id + "*" + array;
   if (sym->attributes[ATTR_void])
      return "void";
   return "ERROR: this should not have happend";
}

void indent (void) {
   fprintf(oilFile, "        ");
}

void print_structdefs (void) {
   for (auto iter = type_names->begin(); iter != type_names->end(); 
         ++iter) {
      fprintf(oilFile, "struct s_%s;\n", iter->first->c_str());
   }
   fprintf(oilFile, "\n");
   for (auto iter = type_names->begin(); iter != type_names->end(); 
         ++iter) {            
      fprintf(oilFile, "struct s_%s {\n", iter->first->c_str());
      symbol_table* fields = iter->second->fields;
      for (auto inner = fields->begin(); inner != fields->end(); 
            ++inner) {
         string type = get_type(inner->second);
         indent();
         fprintf(oilFile, "%s f_%s_%s;\n", type.c_str(), 
            iter->first->c_str(), 
            inner->first->c_str());
      }
      fprintf(oilFile, "};\n");
   }
   fprintf(oilFile, "\n");
}

void print_stringcons (void) { 
   for (size_t i = 0; i < stringConsVector.size(); ++i) {      
      fprintf(oilFile, "char* s%zu = %s;\n", i, 
         stringConsVector[i]->lexinfo->c_str());
   }
   fprintf(oilFile, "\n");
}

void print_golbal_decls (void) { 
   if (symbol_stack.back() != nullptr) {
      for (auto iter = symbol_stack.back()->begin(); 
            iter != symbol_stack.back()->end(); ++iter) {
         if (iter->second->parameters != nullptr)
            continue;
         string type = get_type(iter->second);
         fprintf(oilFile, "%s __%s;\n", type.c_str(), 
            iter->first->c_str());
      }
   }
   fprintf(oilFile, "\n");
}

string get_type (astree* node) {   
   if (node->symbol == TOK_ARRAY) 
      return get_type(node->children[1]->sym_node);
   else 
      return get_type(node->children[0]->sym_node);
}

string get_name (astree* node) {   
   if (node->symbol == TOK_ARRAY) 
      return *node->children[1]->lexinfo;
   else 
      return *node->children[0]->lexinfo;
}

void print_prototypes (void) {
   if (symbol_stack.back() != nullptr) {
      for (auto iter = symbol_stack.back()->begin(); 
            iter != symbol_stack.back()->end(); ++iter) {
         if (iter->second->parameters == nullptr)
            continue;
         string type = get_type(iter->second);
         fprintf(oilFile, "%s __%s (\n", type.c_str(), 
            iter->first->c_str());
         indent();
         bool first = true;
         for (auto &inner : *iter->second->parameters) {
            if (not first) {
               fprintf(oilFile, ",\n");
               indent();
            }
            first = false;
            type = get_type(inner);
            fprintf(oilFile, "%s", type.c_str());
         }
         fprintf(oilFile, ")\n;\n");
      }
   }
   fprintf(oilFile, "\n");
}

void print_functions (astree* node) {
   for (auto &child : node->children) {
      if (child->symbol != TOK_FUNCTION) 
         continue;
      string type  = get_type(child->children[0]);
      string ident = get_name(child->children[0]);
      fprintf(oilFile, "%s __%s (\n", type.c_str(), ident.c_str());
      indent();
      bool first = true;
      for (auto &param : child->children[1]->children) {
         if (not first) {
            fprintf(oilFile, ",\n");
            indent();
         }
         first = false;
         type  = get_type(param);
         ident = get_name(param);
         fprintf(oilFile, "%s _%zu_%s", type.c_str(), 
            child->children[1]->children[0]->blocknr, ident.c_str());
      }
      fprintf(oilFile, ")\n{\n");
      
      // print block
      print_stmt(child->children[2]);
      
      fprintf(oilFile, "}\n");      
   } 
   fprintf(oilFile, "\n");
}

void print_while(astree* node) {
   fprintf(oilFile, "while_%zu_%zu_%zu:;\n", 
      node->lloc.filenr, node->lloc.linenr, node->lloc.offset);
   string operand = emit(node->children[0]);
   indent();
   fprintf(oilFile, "if (!%s) goto break_%zu_%zu_%zu;\n", 
      operand.c_str(),
      node->lloc.filenr, node->lloc.linenr, node->lloc.offset);
   if (node->children.size() > 1)
      print_stmt(node->children[1]);
   indent();
   fprintf(oilFile, "goto while_%zu_%zu_%zu;\n",
      node->lloc.filenr, node->lloc.linenr, node->lloc.offset);
   fprintf(oilFile, "break_%zu_%zu_%zu:;\n",
      node->lloc.filenr, node->lloc.linenr, node->lloc.offset);   
}

void print_if(astree* node) {
   string operand = emit(node->children[0]);
   indent();
   if (node->children.size() > 2) {
      fprintf(oilFile, "if (!%s) goto else_%zu_%zu_%zu;\n", 
         operand.c_str(),
         node->lloc.filenr, node->lloc.linenr, node->lloc.offset);
   } else {
      fprintf(oilFile, "if (!%s) goto fi_%zu_%zu_%zu;\n", 
         operand.c_str(),
         node->lloc.filenr, node->lloc.linenr, node->lloc.offset);      
   }
   print_stmt(node->children[1]);
   if (node->children.size() > 2) {
      indent();
      fprintf(oilFile, "goto fi_%zu_%zu_%zu;\n",
         node->lloc.filenr, node->lloc.linenr, node->lloc.offset);
      fprintf(oilFile, "else_%zu_%zu_%zu:;\n",
         node->lloc.filenr, node->lloc.linenr, node->lloc.offset);
      print_stmt(node->children[2]);
   } 
   fprintf(oilFile, "fi_%zu_%zu_%zu:;\n",
      node->lloc.filenr, node->lloc.linenr, node->lloc.offset);
}

void print_vardecl (astree* node) {   
   string operand = emit(node->children[1]);
   indent();
   string name = get_name(node->children[0]);
   if (node->blocknr != 0) {
      string type = get_type(node->children[0]);
      fprintf(oilFile, "%s _%zu_%s = %s;\n", type.c_str(), 
         node->blocknr, name.c_str(), operand.c_str());
   } else {
      // cout << node->children[0]->lexinfo << " " 
      // << node->children[0]->children[0]->sym_node->blocknr << endl;
      fprintf(oilFile, "__%s = %s;\n", name.c_str(), operand.c_str());
   }    
}

void print_stmt (astree* stmt) {
   switch (stmt->symbol) {
      // Took care of these already
      case TOK_STRUCT:
      case TOK_FUNCTION:
      case TOK_PROTOTYPE:
         break;
      case ROOT:
      case TOK_BLOCK:      
         for (auto &node : stmt->children) {
            print_stmt(node);
         }
         break;
      case TOK_VARDECL:
         // special case for block 0 is handled inside this funciton
         print_vardecl(stmt);
         break;
      case TOK_WHILE:
         print_while(stmt);
         break;
      case TOK_IF:
         print_if(stmt);
         break;
      case TOK_RETURN: {
         string operand = emit(stmt->children[0]);
         indent();
         fprintf(oilFile, "return %s;\n", operand.c_str());
         break;
      }            
      case TOK_RETURNVOID:
         indent();
         fprintf(oilFile, "return;\n");
         break;
      case ';':
         break;
      // must be an expression
      default:
         emit(stmt);  
   }            
}

string print_call (astree* node) {  
   string vreg = "";
   string args[node->children.size()];
   for (size_t i = 1; i < node->children.size(); ++i) {
      args[i] = emit (node->children[i]);
   }
   indent();
   string type = get_type(node->children[0]->sym_node);
   if (type != "void") {
      if (type == "int")
         vreg = "i" + to_string(i++);
      else 
         vreg = "p" + to_string(p++);
      fprintf(oilFile, "%s %s = ", type.c_str(), vreg.c_str());
   }
   fprintf(oilFile, "__%s (", node->children[0]->lexinfo->c_str());
   bool first = true;
   for (size_t i = 1; i < node->children.size(); ++i) {
      if (not first)
         fprintf(oilFile, ", ");
      fprintf(oilFile, "%s", args[i].c_str());
      first = false;
   }
   fprintf(oilFile, ");\n");
   node->vreg = vreg;
   return vreg;
}

string math_op (astree* node) {   
   string left = emit (node->children[0]);
   string right = emit (node->children[1]);
   string vreg = "i" + to_string(i++);
   indent();
   fprintf(oilFile, "int %s = %s %c %s;\n", vreg.c_str(), 
      left.c_str(), node->symbol, right.c_str());
   node->vreg = vreg;
   return vreg;
}

string uni_op (astree* node) {   
   string right = emit (node->children[0]);
   string vreg = "i" + to_string(i++);
   string op;
   switch (node->symbol) {
      case TOK_POS:
         op = "+";
         break;
      case TOK_NEG:
         op = "-";
         break;
      default:
         op = "!";
   }
   indent();
   fprintf(oilFile, "int %s = %s%s;\n", vreg.c_str(), 
      op.c_str(), right.c_str());
   node->vreg = vreg;
   return vreg;
}

string comp_op (astree* node) {
   string left = emit (node->children[0]);
   string right = emit (node->children[1]);
   string op;
   string vreg = "i" + to_string(i++);
   switch (node->symbol) {
      case TOK_EQ:
         op = "==";
         break;
      case TOK_NE: 
         op = "!=";
         break;
      case TOK_LT: 
         op = "<";
         break;
      case TOK_LE:
         op = "<=";
         break;
      case TOK_GT: 
         op = ">";
         break;
      case TOK_GE:
         op = ">=";
         break;
   }
   indent();
   fprintf(oilFile, "int %s = %s %s %s;\n", vreg.c_str(), left.c_str(), 
      op.c_str(), right.c_str());
   node->vreg = vreg;
   return vreg;
}

string print_newobject (astree* node) {
   indent();
   const char* type = node->children[0]->lexinfo->c_str(); 
   string vreg = "p" + to_string(p++);
   fprintf(oilFile, 
      "struct s_%s* %s = xcalloc (1, sizeof (struct s_%s));\n", 
      type, vreg.c_str(), type);
   node->vreg = vreg;
   return vreg;
}

string print_newstring (astree* node) {
   string operand = emit (node->children[0]);
   indent();
   string vreg = "p" + to_string(p++);
   fprintf(oilFile, "char* %s = xcalloc (%s, sizeof (char));\n",
      vreg.c_str(), operand.c_str());
   node->vreg = vreg;
   return vreg;
}

string print_newarray (astree* node) {
   string operand = emit (node->children[1]);
   string type;
   switch (node->children[0]->symbol) {
      case TOK_INT:
         type = "int";
         break;
      case TOK_STRING:
         type = "char*";
         break;
      case TOK_TYPEID:
         type = "struct " + *node->children[0]->lexinfo + "*";
         break;
      default:
         type = "ERROR: in new array";
   }
   indent();
   string vreg = "p" + to_string(p++);
   fprintf(oilFile, "%s* %s = xcalloc (%s, sizeof (%s));\n",
      type.c_str(), vreg.c_str(), operand.c_str(), type.c_str());
   node->vreg = vreg;
   return vreg;
}

string print_index (astree* node) {
   string left = emit (node->children[0]);
   string right = emit (node->children[1]);
   string type;
   // accessing char in int
   if (not node->children[0]->attributes[ATTR_array])
      type = "char*";
   else if (node->attributes[ATTR_string])
      type = "char**";
   else if (node->attributes[ATTR_int])
      type = "int*";
   else if (node->attributes[ATTR_struct])
      type = "struct " + *node->type_id + "**";
   indent();
   string vreg = "a" + to_string(a++);
   fprintf(oilFile, "%s %s = &%s[%s];\n", type.c_str(), vreg.c_str(),
      left.c_str(), right.c_str());
   node->vreg = "*" + vreg;   // should also wrok without the *
   return "(*" + vreg + ")";   
}

string print_selector (astree* node) {
   string ident = emit (node->children[0]); 
   string field = "f_" + *node->children[0]->type_id + "_" + 
      *node->children[1]->lexinfo;
   string type;
   if (node->attributes[ATTR_string])
      type = "char*";
   else if (node->attributes[ATTR_int])
      type = "int";
   else if (node->attributes[ATTR_struct])
      type = "struct s_" + *node->type_id + "*";
   if (node->attributes[ATTR_array])
      type += "*";
   indent();
   string vreg = "a" + to_string(a++);
   fprintf(oilFile, "%s* %s = &%s->%s;\n", type.c_str(), vreg.c_str(),
      ident.c_str(), field.c_str());
   node->vreg = "*" + vreg;   // should also wrok without the *
   return "(*" + vreg + ")";   
}

string mangle_ident (astree* node) {   
   if (node->sym_node == nullptr)
      return "Error no symbol";
   if (node->sym_node->blocknr != 0) 
      return "_" + to_string(node->sym_node->blocknr) + 
         "_" + *node->lexinfo;
   else 
      return "__" + *node->lexinfo;
}

string print_asignment (astree* node) {
   string left = emit (node->children[0]);
   string right = emit (node->children[1]);
   indent();
   fprintf(oilFile, "%s = %s;\n", left.c_str(), right.c_str());
   node->vreg = node->children[0]->vreg;
   return node->children[0]->vreg;
}

string emit (astree* node) {
   
   switch (node->symbol) {
      case TOK_NEW:
         return print_newobject (node);        
      case TOK_NEWSTRING:
         return print_newstring (node);
      case TOK_NEWARRAY:
         return print_newarray (node);
      case TOK_CALL:
         return print_call (node);
         
      case TOK_INDEX:
         return print_index (node);
      case '.':
         return print_selector (node);
         
      case '=':
         return print_asignment (node);
      
      // base cases
      case TOK_IDENT:
         return mangle_ident (node);
      case TOK_INTCON: 
         if (*node->lexinfo == "0")
            return "0";
         else 
            return (*node->lexinfo).substr(
               (*node->lexinfo).find_first_not_of("0"));
      case TOK_CHARCON:
         return (*node->lexinfo) ;
      case TOK_STRINGCON:
         for (size_t i = 0; i < stringConsVector.size(); ++i) { 
            if (node == stringConsVector[i])
               return "s" + to_string(i);
         }
      case TOK_NULL:
         return "0";
      
      // operators
      case TOK_EQ:
      case TOK_NE: 
      case TOK_LT: 
      case TOK_LE:
      case TOK_GT: 
      case TOK_GE:
         return comp_op (node);      
      case '+':
      case '-':
      case '*':
      case '/':
      case '%':
         return math_op (node);
      case TOK_POS:
      case TOK_NEG:
      case '!':
         return uni_op (node);
         
      // error
      default:
         return "default";
   }
   
}

int emit_code(astree* node) {

   i = a = p = 0;

   fprintf(oilFile, "#define __OCLIB_C__\n");
   fprintf(oilFile, "#include \"oclib.oh\"\n");
   
   print_structdefs();   
   print_stringcons();
   print_golbal_decls();
   // print_prototypes();
   print_functions(node);
   
   fprintf(oilFile, "void __ocmain (void) {\n");
   
   // print all statments (root is handled as if it was a block)
   print_stmt(node);
   
   fprintf(oilFile, "}\n");
   
   if (exec::exit_status != EXIT_SUCCESS)
      fprintf(oilFile, "Syntax and or typchecking Errors found:\n");
   
   return 0;
}
