// $Id: emitter.h,v 1.1 2017-04-09 17:04:29-07 - - $

#ifndef __EMITTER_H__
#define __EMITTER_H__

#include <string>
#include <vector>
#include <bitset>
#include <unordered_map>
#include <iostream>

using namespace std;

extern FILE* oilFile;


int emit_code(astree* node);

#endif
