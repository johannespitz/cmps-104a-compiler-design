// $Id: symbol.cpp,v 1.1 2017-04-09 17:04:29-07 - - $

#include "astree.h"
#include "lyutils.h"
#include "symbol.h"
#include "string_set.h"
#include <iostream>

// for variables and function
vector<symbol_table*> symbol_stack;
// for structs
symbol_table* type_names;

size_t next_block;
vector<size_t> block_stack;

FILE* symFile;
vector<astree*> stringConsVector;



symbol* cur_func = nullptr;
bool newFunc = false;

// to print out bitset easier (also in astree.cpp)
vector<string> attr_names = {
   "void",
   "int",
   "null",
   "string",
   "struct",
   "array",
   "function",
   "variable",
   "field",
   "typeid",
   "param",
   "lval",
   "const",
   "vreg",
   "vaddr",
   "function",
   "bitset_size",
};

symbol::symbol(astree* node, size_t _blocknr) {
   attributes = node->attributes;
   fields = nullptr;
   lloc = node->lloc;
   blocknr = _blocknr;
   parameters = nullptr;
   type_id = nullptr;
}

bool is_compatible(attr_bitset attr1, const string* type1, 
                  attr_bitset attr2, const string* type2) {
   if((attr1[ATTR_null] and attr2[ATTR_struct]) or 
      (attr1[ATTR_struct] and attr2[ATTR_null]))
      return true;
   if((attr1[ATTR_null] and attr2[ATTR_string]) or 
      (attr1[ATTR_string] and attr2[ATTR_null]))
      return true;
   if((attr1[ATTR_null] and attr2[ATTR_array]) or 
      (attr1[ATTR_array] and attr2[ATTR_null]))
      return true;
   if (attr1[ATTR_int] != attr2[ATTR_int])
      return false;
   if (attr1[ATTR_string] != attr2[ATTR_string])
      return false;
   if (attr1[ATTR_array] != attr2[ATTR_array])
      return false;
   if (attr1[ATTR_struct] != attr2[ATTR_struct])
      return false;
   if (type1 != type2)
      return false;

   return true;
}

int check_return_void(astree* node) {
   if(cur_func == nullptr) {
      return 0;
   }

   if(cur_func->attributes[ATTR_void] == false) {
      errllocprintf(node->lloc, "%s\n", 
         "Error return void in non-void function");
      return -1;
   }

   return 0;
}

int check_return_type(astree* node) {
   if(cur_func == nullptr) {
      errllocprintf(node->lloc, "%s\n", 
         "Error return outside func");
      return -1;
   }

   astree* type = node->children[0];

   bool compatible = is_compatible(type->attributes, 
     type->type_id,
     cur_func->attributes,
     cur_func->type_id);

   if(not compatible) {
      errllocprintf(node->lloc, "%s\n", 
         "Error return type does not match");
      return -1;
   }

   return 0;
}

int insert_struct(astree* node) { 
   
   astree* struct_name_p = node->children[0];

   symbol_table::iterator search = 
      type_names->find(struct_name_p->lexinfo); 
   if (search != type_names->end()) {   
         errllocprintf(node->lloc, "%s\n", 
            "Error struct redefined");
         return -1;         
   }

   symbol* sym = new symbol(node, node->blocknr); 
   sym->attributes[ATTR_struct] = true;
   sym->type_id = struct_name_p->lexinfo;    
   
   symbol_table* fields = new symbol_table();
   type_names->emplace(struct_name_p->lexinfo, sym);   
   
   fprintf(symFile, "%s (%zu.%zu.%zu) {%zu} struct \"%s\"\n", 
      struct_name_p->lexinfo->c_str(),
      node->lloc.filenr,
      node->lloc.linenr,
      node->lloc.offset,
      node->blocknr,
      sym->type_id->c_str()
   );
   
   for (unsigned int i = 1; i < node->children.size(); ++i) { 
   
      symbol* _sym = 
         new symbol(node->children[i]->children[0], 1);          
   
      astree* basetype_p;
      astree* field_name_p;
   
      if(node->children[i]->symbol == TOK_ARRAY) {
         _sym->attributes[ATTR_array] = true;
         basetype_p = node->children[i]->children[0];
         field_name_p = node->children[i]->children[1];
         if (basetype_p->symbol == TOK_VOID) {
            errllocprintf(node->lloc, "%s\n",
               "Error array of void is not allowed");
         }
      } else {
         basetype_p = node->children[i];
         field_name_p = node->children[i]->children[0];
      }

      symbol_table::iterator search = 
         fields->find(field_name_p->lexinfo);
      if (search != fields->end()) {
         errllocprintf(field_name_p->lloc, "%s\n", 
            "Error field name already exists");
         return -1;
      }

      if(basetype_p->symbol == TOK_TYPEID) {
         symbol_table::iterator search = 
            type_names->find(basetype_p->lexinfo);
         if(search != type_names->end()) {
            free(_sym);
            _sym = search->second;
         }
      }
      
      set_basetype(_sym, basetype_p);
      _sym->attributes[ATTR_field] = true;
      fields->emplace(field_name_p->lexinfo, _sym);
      
      fprintf(symFile, "%*c", 3, ' ');
      fprintf(symFile, "%s (%zu.%zu.%zu) field {%s}", 
         field_name_p->lexinfo->c_str(),
         field_name_p->lloc.filenr,
         field_name_p->lloc.linenr,
         field_name_p->lloc.offset,
         struct_name_p->lexinfo->c_str()
      );
      
      for (int j = 0; j < ATTR_bitset_size; ++j) {
         if(j == ATTR_struct and _sym->attributes[j]) {
            fprintf(symFile, " struct \"%s\"",  _sym->type_id->c_str());
            continue;
         }
         if (_sym->attributes[j] and j != ATTR_field) {
            fprintf(symFile, " %s", attr_names[j].c_str());
         }
      }      
      fprintf(symFile, "\n");
      field_name_p->attributes = _sym->attributes;
      field_name_p->type_id = _sym->type_id;
      field_name_p->sym_node = _sym;
   }   
   fprintf(symFile, "\n");

   sym->fields = fields;
   struct_name_p->attributes = sym->attributes;
   struct_name_p->type_id = sym->type_id;
   struct_name_p->sym_node = sym; 

   for (auto it = type_names->begin(); it != type_names->end(); ++it) {
      if(it->first == sym->type_id or it->second->fields == nullptr) 
         continue;
      for(auto sr = it->second->fields->begin(); 
         sr != it->second->fields->end(); ++sr) {
         if(sr->second->attributes[ATTR_struct] 
            and sr->second->fields == nullptr  
            and sr->second->type_id == sym->type_id) {
            sr->second = sym;
         }
      }
   }

   return 0;
} 

bool is_struct_complete(symbol* sym) {
   if (sym->fields == nullptr) {
      return false;
   }
   for(auto sr = sym->fields->begin(); 
      sr != sym->fields->end(); ++sr) {
      if(sr->second->attributes[ATTR_struct]) {
         // could make recursive call in here if wanted
         if (sr->second->fields == nullptr) {
            return false;
         }
      }
   }

   return true;
}

int set_basetype (symbol* sym, astree* basetype) {
   
   switch (basetype->symbol) {
      case TOK_INT:
         sym->attributes[ATTR_int] = true;
         break;
      case TOK_STRING:
         sym->attributes[ATTR_string] = true;
         break;
      case TOK_TYPEID:
         sym->attributes[ATTR_struct] = true;   
         sym->type_id = basetype->lexinfo;
         break;
      case TOK_VOID:
         errllocprintf(basetype->lloc, "%s\n", 
            "Error tried to declare a field as void");
         return -1;
      default:
         return -1;
   }   

   return 0;
}

int insert_vardecl(astree* node) {
   
   symbol* sym = 
      new symbol(node, block_stack.back());          

   astree* basetype_p;
   astree* var_name_p;

   if(node->children[0]->symbol == TOK_ARRAY) {
      sym->attributes[ATTR_array] = true;
      basetype_p = node->children[0]->children[0];
      var_name_p = node->children[0]->children[1];
      if (basetype_p->symbol == TOK_VOID) {
         errllocprintf(node->lloc, "%s\n",
            "Error array of void is not allowed");
      }
   } else {
      basetype_p = node->children[0];
      var_name_p = node->children[0]->children[0];
   }
      
   if (symbol_stack.back() == nullptr) {
      symbol_stack.pop_back();
      symbol_table* table = new symbol_table();
      symbol_stack.push_back(table);
   } 

   symbol_table::iterator search = 
      symbol_stack.back()->find(var_name_p->lexinfo); 
   if (search != symbol_stack.back()->end()) {  
      errllocprintf(node->lloc, "%s\n", 
         "Error multiple declarations of variable");
      free(sym);
      return -1;
   }
   
   set_basetype(sym, basetype_p);

   sym->attributes[ATTR_variable] = true;
   sym->attributes[ATTR_lval] = true;
   
   if (basetype_p->symbol == TOK_TYPEID) {
      if (type_names->find(basetype_p->lexinfo) == type_names->end()) {
         errllocprintf(node->lloc, "%s\n", 
            "Error struct not declared");
         return -1;
      }
      if (not is_struct_complete
            (type_names->find(basetype_p->lexinfo)->second)) {
         errllocprintf(node->lloc, "%s\n", 
            "Error referrencing incomplete type");
         return -1;
      }
   }
   
   // cpplint complained on cast (int) so we used stat_cast<int>
   int spaces = (static_cast<int>(block_stack.size()) - 1) * 3;
   if(spaces != 0)
      fprintf(symFile, "%*c", spaces, ' ');
   
   fprintf(symFile, "%s (%zu.%zu.%zu) {%zu}", 
      var_name_p->lexinfo->c_str(),
      var_name_p->lloc.filenr,
      var_name_p->lloc.linenr,
      var_name_p->lloc.offset,
      node->blocknr
   );  

   for (int j = 0; j < ATTR_bitset_size; ++j) {      
      if(j == ATTR_struct and sym->attributes[j]) {
         fprintf(symFile, " struct \"%s\"", sym->type_id->c_str());
         continue;
      }      
      if (sym->attributes[j]) {
         fprintf(symFile, " %s", attr_names[j].c_str());
      }
   }      

   fprintf(symFile, "\n");

   symbol_stack.back()->emplace(var_name_p->lexinfo, sym);
   var_name_p->attributes = sym->attributes;
   var_name_p->type_id = sym->type_id;
   var_name_p->sym_node = sym; 

   return 0;
}

int insert_function(astree* node) {
    
   symbol* sym = new symbol(node, node->blocknr);  
   sym->parameters = new vector<symbol*>;

   astree* basetype_p;
   astree* fun_name_p;
   astree* arg_p = node->children[1];    
   
   if(node->children[0]->symbol == TOK_ARRAY) {
      sym->attributes[ATTR_array] = true;
      basetype_p = node->children[0]->children[0];
      fun_name_p = node->children[0]->children[1];
      if (basetype_p->symbol == TOK_VOID) {
         errllocprintf(node->lloc, "%s\n",
            "Error array of void is not allowed");
      }
   } else {
      basetype_p = node->children[0];
      fun_name_p = node->children[0]->children[0];
   }     
      
   if (symbol_stack.back() == nullptr) {
      symbol_stack.pop_back();
      symbol_table* table = new symbol_table();
      symbol_stack.push_back(table);
   } 

   if(basetype_p->symbol != TOK_VOID)
      set_basetype(sym, basetype_p);
   else
      sym->attributes[ATTR_void] = true;
   
   if(node->children.size() == 3) {
      // remember symbol for function for future TOK_RETURN
      cur_func = sym;
      sym->attributes[ATTR_function] = true;
   } else {
      sym->attributes[ATTR_prototype] = true;
   }
   
   bool first = 
      symbol_stack[0]->find(fun_name_p->lexinfo) 
      == symbol_stack[0]->end(); 
   newFunc = first;

   if(first) {
      fprintf(symFile, "%s (%zu.%zu.%zu) {%zu}", 
         fun_name_p->lexinfo->c_str(),
         fun_name_p->lloc.filenr,
         fun_name_p->lloc.linenr,
         fun_name_p->lloc.offset,
         node->blocknr
      );
   
      for (int j = 0; j < ATTR_bitset_size; ++j) {      
         if(j == ATTR_struct and sym->attributes[j]) {
            fprintf(symFile, " struct \"%s\"", sym->type_id->c_str());
            continue;
         }      
         if (sym->attributes[j]) {
            fprintf(symFile, " %s", attr_names[j].c_str());
         }
      }      
      fprintf(symFile, "\n");
   }

   symbol_table* table = new symbol_table();
   symbol_stack.push_back(table);
   block_stack.push_back(next_block++);
  
   for (unsigned int i = 0; i < arg_p->children.size(); ++i) { 

      symbol* _sym = 
         new symbol(arg_p->children[i]->children[0], 
            block_stack.back());  
   
      astree* arg_basetype_p;
      astree* arg_name_p;
   
      if(arg_p->children[i]->symbol == TOK_ARRAY) {
         _sym->attributes[ATTR_array] = true;
         arg_basetype_p = arg_p->children[i]->children[0];
         arg_name_p = arg_p->children[i]->children[1];
         if (arg_basetype_p->symbol == TOK_VOID) {
            errllocprintf(node->lloc, "%s\n",
               "Error array of void is not allowed");
         }
      } else {
         arg_basetype_p = arg_p->children[i];
         arg_name_p = arg_p->children[i]->children[0];
      }   
   
      if(arg_basetype_p->symbol == TOK_TYPEID) {
         symbol_table::iterator search = 
            type_names->find(arg_basetype_p->lexinfo); 
         if (search == type_names->end()) {  
            errllocprintf(node->lloc, "%s\n", 
               "Error struct not yet defined");
            return -1;
         }
      }
      
      set_basetype(_sym, arg_basetype_p);
      _sym->attributes[ATTR_param] = true;
      _sym->attributes[ATTR_variable] = true;
      _sym->attributes[ATTR_lval] = true;
      
      symbol_table::iterator search = 
         symbol_stack.back()->find(arg_name_p->lexinfo);
      if (search != symbol_stack.back()->end()) { 
         errllocprintf(node->lloc, "%s\n", 
            "Error same name in same block");
         return -1;
      }

      symbol_stack.back()->emplace(arg_name_p->lexinfo, _sym);    
      sym->parameters->push_back(_sym);
      
      if(first) {
         int spaces = 3;
         fprintf(symFile, "%*c", spaces, ' ');
         fprintf(symFile, "%s (%zu.%zu.%zu) {%zu}", 
            arg_name_p->lexinfo->c_str(),
            arg_name_p->lloc.filenr,
            arg_name_p->lloc.linenr,
            arg_name_p->lloc.offset,
            //the real blocknr for this node 
            // (arg_name_p) will be set later
            node->blocknr + 1
         );
      
         for (int j = 0; j < ATTR_bitset_size; ++j) {
            if(j == ATTR_struct and _sym->attributes[j]) {
               fprintf(symFile, " struct \"%s\"",  
                  _sym->type_id->c_str());
               continue;
            }
            if (_sym->attributes[j]) {
               fprintf(symFile, " %s", attr_names[j].c_str());
            }
         }      
         fprintf(symFile, "\n");
      }
      arg_name_p->attributes = _sym->attributes;
      arg_name_p->type_id = _sym->type_id;
      arg_name_p->sym_node = _sym;
   }

   symbol_table::iterator search = 
      symbol_stack[0]->find(fun_name_p->lexinfo); 
   if (search != table->end()) {  

      if (search->second->attributes[ATTR_prototype] 
            and sym->attributes[ATTR_function]) {

         attr_bitset tmp {};
         tmp.set(ATTR_prototype, 1);
         tmp.set(ATTR_function, 1);
         tmp.flip();

         if((tmp & (sym->attributes ^ search->second->attributes)) 
            != 0) {
            errllocprintf(node->lloc, "%s\n", 
               "Error function and prototype different attrs");
            return -1;
         }
         unsigned int size1 = search->second->parameters->size();
         unsigned int size2 = sym->parameters->size();
         if(size1 != size2) {
            errllocprintf(node->lloc, "%s\n", 
               "Error same function name with different parameter list"
               " sizes");
            return -1;
         }
         for(unsigned int k = 0; k < size1; ++k) {
            if(((search->second->parameters->at(k)->attributes 
               ^ sym->parameters->at(k)->attributes) != 0) or 
               (search->second->parameters->at(k)->type_id 
               != sym->parameters->at(k)->type_id)) {
               errllocprintf(node->lloc, "%s\n", 
                  "Error parameters of prototype and function do not"
                  " match types");
               return -1;
            }
         }
         search->second->attributes[ATTR_prototype] = false;
         search->second->attributes[ATTR_function] = true;
         fun_name_p->attributes = search->second->attributes;
         fun_name_p->type_id = search->second->type_id;
         fun_name_p->sym_node = search->second;
      } else if(search->second->attributes[ATTR_function] 
         and sym->attributes[ATTR_function]) {
         errllocprintf(node->lloc, "%s\n", 
            "Error double definition of function");
         return -1;
         free(sym);
      } else if(search->second->attributes[ATTR_function] 
         and sym->attributes[ATTR_prototype]) {
         errllocprintf(node->lloc, "%s\n", 
            "Error prototype for already defined function");
         return -1;
      } else if(search->second->attributes[ATTR_prototype] 
         and sym->attributes[ATTR_prototype]) {
         errllocprintf(node->lloc, "%s\n", 
            "Error double prototype for same function signature");
         return -1;
      } else {
         errllocprintf(node->lloc, "%s\n", 
            "Error declaring function with same name as variable");
         return -1;
      }
   } else {
      symbol_stack[0]->emplace(fun_name_p->lexinfo, sym);
      fun_name_p->attributes = sym->attributes;
      fun_name_p->type_id = sym->type_id;
      fun_name_p->sym_node = sym;  
   }

   if(first)
      fprintf(symFile, "\n");
   
   return 0;
}

int find_ident (astree* node) { 
   
   for (auto iter = symbol_stack.rbegin(); 
         iter != symbol_stack.rend(); ++iter) {
      if (*iter == nullptr)
         continue;
      symbol_table::iterator search = (*iter)->find(node->lexinfo); 
      if (search != (*iter)->end()) { 
         node->attributes = search->second->attributes;
         node->type_id = search->second->type_id;
         node->sym_node = search->second;
         return 0;
      }  
   }

   errllocprintf(node->lloc, "Error identifier %s not defined\n", 
      node->lexinfo->c_str());

   return 0;
}   

int access_field (astree* node) {
   
   const string* field_name = node->children[1]->lexinfo;
   
   for (auto iter = symbol_stack.rbegin(); 
         iter != symbol_stack.rend(); ++iter) {
      if (*iter == nullptr)
         continue;
      if(node->children[0]->type_id == nullptr) {
         errllocprintf(node->lloc, "%s\n", "Error no type-id");
         continue;
      }
      const string* type = node->children[0]->type_id;
      symbol_table::iterator search = type_names->find(type);
      if (search == type_names->end()) {
         errllocprintf(node->lloc, "Error trying to access a field of" 
            "non struct type %s: %s\n",
            type->c_str());
         return -1;
      } 

      if (not is_struct_complete(search->second)) {
         errllocprintf(node->lloc, "%s\n", 
            "Error referrencing incomplete type");
         return -1;
      }

      symbol_table::iterator prev = search;
      search = search->second->fields->find(field_name);
      if (search == prev->second->fields->end()) {
         errllocprintf(node->lloc, "%s\n", 
            "Error field does not exist in this struct");
         return -1;
      }    
      node->attributes = search->second->attributes;
      node->attributes[ATTR_field] = false;
      node->attributes[ATTR_variable] = false;     
      node->attributes[ATTR_vaddr] = true;
      node->attributes[ATTR_lval] = true; 
      node->type_id = search->second->type_id;  

      return 0;
   }  
   
   errllocprintf(node->lloc, "%s\n", 
      "Error trying to access a field of non struct type");

   return -1;
}
  

int access_array(astree* node) {
   
   astree* expr = node->children[1]; 

   if (not expr->attributes[ATTR_int]
         or expr->attributes[ATTR_array]) {
      errllocprintf(node->lloc, "%s\n", 
         "Error accessing array with non integer");
   }

   if (not node->children[0]->attributes[ATTR_array]) {
      if (node->children[0]->attributes[ATTR_string]) {
         node->attributes[ATTR_int] = true;
         node->attributes[ATTR_vaddr] = true;
         node->attributes[ATTR_lval] = true;
         return 0;
      }
      errllocprintf(node->lloc, "%s\n", 
         "Error accessing non array type");
      return -1;
   }

   node->attributes[ATTR_int] = 
      node->children[0]->attributes[ATTR_int];
   node->attributes[ATTR_string] = 
      node->children[0]->attributes[ATTR_string];
   node->attributes[ATTR_struct] = 
      node->children[0]->attributes[ATTR_struct];

   node->attributes[ATTR_array] = false;
   node->attributes[ATTR_variable] = false;
   node->attributes[ATTR_vaddr] = true;
   node->attributes[ATTR_lval] = true;
   node->type_id = node->children[0]->type_id;

   return 0;
}

int new_string(astree* node) {

   if (not node->children[0]->attributes[ATTR_int]
         or node->children[0]->attributes[ATTR_array]) {
      errllocprintf(node->lloc, "%s\n", 
         "Error new string() needs integer size");
      return -1;
   }

   node->attributes[ATTR_string] = true;
   node->attributes[ATTR_vreg] = true;

   return 0;
}

int new_array (astree* node) {
   
   astree* basetype_p = node->children[0];

   if(basetype_p->symbol == TOK_ARRAY) {
      errllocprintf(node->lloc, "%s\n", 
         "Error arrays of arrays are not allowed");
   }   
   if(basetype_p->symbol == TOK_TYPEID) {
      symbol_table::iterator search = 
         type_names->find(basetype_p->lexinfo); 
      if (search == type_names->end()) {  
         errllocprintf(basetype_p->lloc, "%s\n", 
            "Error struct not yet defined");         
         return -1;
      }
   }     
   switch (basetype_p->symbol) {
      case TOK_INT:
         node->attributes[ATTR_int] = true;
         break;
      case TOK_STRING:
         node->attributes[ATTR_string] = true;
         break;
      case TOK_TYPEID:
         node->attributes[ATTR_struct] = true;   
         node->type_id = basetype_p->lexinfo;
         break;
      case TOK_VOID:
         errllocprintf(basetype_p->lloc, "%s\n", 
            "Error tried to declare array of type void");
         return -1;
      default:
         errllocprintf(basetype_p->lloc, "%s\n", 
            "Error tried to declare array of on basetype");
         return -1;
   }     
   if (not node->children[1]->attributes[ATTR_int]
         or node->children[1]->attributes[ATTR_array]) {
      errllocprintf(node->lloc, "%s\n", 
         "Error new array[] needs integer size");
      return -1;
   }     
   node->attributes[ATTR_array] = true;
   node->attributes[ATTR_vreg] = true;

   return 0;
}

int new_object (astree* node) {
     
   symbol_table::iterator search = 
      type_names->find(node->children[0]->lexinfo); 
   if (search == type_names->end()) {  
      errllocprintf(node->children[0]->lloc, "%s\n", 
         "Error struct not yet defined");         
      return -1;
   }  
   if (not is_struct_complete(search->second)) {
      errllocprintf(node->lloc, "%s\n", 
         "Error referrencing incomplete type");
      return -1;
   }

   node->attributes[ATTR_struct] = true;   
   node->type_id = node->children[0]->lexinfo;
   node->attributes[ATTR_vreg] = true;  

   return 0;
}

int make_call (astree* node) {
   
   astree* fun_name_p = node->children[0];  

   symbol_table::iterator search = 
      symbol_stack[0]->find(fun_name_p->lexinfo);
   if (search == symbol_stack[0]->end()) {  
      errllocprintf(fun_name_p->lloc, "%s\n", 
         "Function is not declared");         
      return -1;
   }  
   symbol* sym = search->second;
   if (sym->parameters == nullptr) { 
      errllocprintf(fun_name_p->lloc, "%s\n", 
         "Trying to call a non function type"); 
      return -1;
   }

   unsigned int size1 = node->children.size() - 1;
   unsigned int size2 = sym->parameters->size();
   if(size1 != size2) {
      errllocprintf(node->lloc, "%s\n", 
         "Error calling function with different parameter list"
         "sizes");
      return -1;
   }

   for(unsigned int k = 0; k < size1; ++k) {
      bool compatible = is_compatible(node->children[k+1]->attributes, 
                    node->children[k+1]->type_id,
                    sym->parameters->at(k)->attributes,
                    sym->parameters->at(k)->type_id);

      if(not compatible) {
         errllocprintf(node->lloc, "%s\n", 
            "Error parameters in call do not match function");
         return -1;
      }
   }
   
   node->attributes = sym->attributes;
   node->attributes[ATTR_function] = false;
   node->attributes[ATTR_vreg] = true;
   node->type_id = sym->type_id;

   return 0;
}


   
int tree_traversal_rec(struct astree* node) {

   node->blocknr = block_stack.back();
   
   switch (node->symbol) {
      case TOK_BLOCK: {
         symbol_stack.push_back(nullptr);
         block_stack.push_back(next_block++);
         break;
      } 
      case TOK_PROTOTYPE:
      case TOK_FUNCTION: {
         if (block_stack.size() != 1) {
            errllocprintf(node->lloc, "%s\n", 
               "Error function declared in non-root block");
            return -1;
         }
         insert_function(node); 
         break;
      }
      case TOK_VARDECL:
         insert_vardecl(node); 
         break;
      case TOK_STRUCT: {
         if (block_stack.size() != 1) {
            errllocprintf(node->lloc, "%s\n", 
               "Error struct declared in non-root block");
            return -1;
         }
         insert_struct(node); 
      }  
      default:
         break;
   }
   
   for (astree* child: node->children) {
      tree_traversal_rec(child);
   }
      
   switch (node->symbol) {
      case TOK_PROTOTYPE:
      case TOK_FUNCTION:
         if(newFunc) {
            fprintf(symFile, "\n");
            newFunc = false;
         }
         cur_func = nullptr;
      case TOK_BLOCK: 
         symbol_stack.pop_back();
         block_stack.pop_back();
         break;

      // return
      case TOK_RETURNVOID: {
         check_return_void(node);
         break;
      }

      case TOK_RETURN: {
         check_return_type(node);
         break;
      }

      case TOK_VARDECL: {         
         astree* var_name_p;
         if (node->children[0]->symbol == TOK_ARRAY)
            var_name_p = node->children[0]->children[1];
         else 
            var_name_p = node->children[0]->children[0];    
         if(var_name_p->sym_node == NULL)
            break;
         bool compatible = 
            is_compatible(var_name_p->sym_node->attributes, 
                          var_name_p->sym_node->type_id,
                          node->children[1]->attributes, 
                          node->children[1]->type_id);
         if(not compatible) {
            errllocprintf(node->lloc, "%s\n", 
               "Error vardecl has incompatible types");
            return -1;
         }
         break;
      }
      
      // allocator
      case TOK_NEW:
         new_object(node);
         break;
      case TOK_NEWARRAY:
         new_array(node);
         break;
      case TOK_NEWSTRING:
         new_string(node);
         break;
      
      // variables
      case TOK_IDENT: 
         find_ident(node); 
         break;
      case TOK_INDEX:
         access_array(node);
         break;
      case '.':
         access_field(node);
         break;
      
      // call
      case TOK_CALL:
         make_call(node);
         break;
         
      // comp operator
      case TOK_EQ: 
      case TOK_NE:
      case TOK_LT:
      case TOK_LE:
      case TOK_GT:
      case TOK_GE:{
         bool compatible = is_compatible(node->children[0]->attributes, 
                                         node->children[0]->type_id,
                                         node->children[1]->attributes, 
                                         node->children[1]->type_id);
         if(not compatible) {
            errllocprintf(node->lloc, "%s\n", 
               "Error vardecl has incompatible types");
            return -1;
         }
         node->attributes[ATTR_int] = true;
         node->attributes[ATTR_vreg] = true;
         break;
      }   

      case '=':{
         bool compatible = is_compatible(node->children[0]->attributes,
                                         node->children[0]->type_id,
                                         node->children[1]->attributes,
                                         node->children[1]->type_id);
         if(compatible && node->children[0]->attributes[ATTR_lval]) {
            node->attributes[ATTR_int] = 
               node->children[0]->attributes[ATTR_int];
            node->attributes[ATTR_string] = 
               node->children[0]->attributes[ATTR_string];
            node->attributes[ATTR_struct] = 
               node->children[0]->attributes[ATTR_struct];
            node->attributes[ATTR_array] = 
               node->children[0]->attributes[ATTR_array];
            node->type_id = node->children[0]->type_id;
            node->attributes[ATTR_vreg] = true;
            break;
         }
         errllocprintf(node->lloc, "%s\n", 
            "Error vardecl has incompatible types");
         break;
      }
         
         
      // operators
      case '+':
      case '-':
      case '*':
      case '/':
      case '%':
         if (node->children[0]->attributes[ATTR_int] 
               and not node->children[0]->attributes[ATTR_array]
               and node->children[1]->attributes[ATTR_int]
               and not node->children[1]->attributes[ATTR_array]) {
            node->attributes[ATTR_int] = true;
            node->attributes[ATTR_vreg] = true;
         } else {
            errllocprintf(node->lloc, "%s\n", 
               "Error binary operator can only be applied to ints");
         } break;
      case TOK_POS:
      case TOK_NEG:
      case '!':
         if (node->children[0]->attributes[ATTR_int]
               and not node->children[0]->attributes[ATTR_array]) {
            node->attributes[ATTR_int] = true;
            node->attributes[ATTR_vreg] = true;
         } else {
            errllocprintf(node->lloc, "%s\n", 
               "Error unary operator can only be applied to int");
         } break;
      
      // constants
      case TOK_INTCON: 
      case TOK_CHARCON: 
         node->attributes[ATTR_int] = true;
         node->attributes[ATTR_const] = true;
         break;
      case TOK_STRINGCON: 
         stringConsVector.push_back(node);
         node->attributes[ATTR_string] = true;
         node->attributes[ATTR_const] = true;
         break;
      case TOK_NULL:
         node->attributes[ATTR_null] = true;
         node->attributes[ATTR_const] = true;
         break;        
      
      // "meta tokens"
      case ROOT:
      case TOK_DECLID:
      case TOK_STRUCT:
      case TOK_PARAMLIST:
      case TOK_ARRAY:
      case TOK_INT:
      case TOK_STRING:
      case TOK_VOID:
      case TOK_TYPEID:
         break;
      case TOK_FIELD:
         node->attributes[ATTR_field] = true;
         break; 
         
      default:
         return 0;
   }
   
   return 0;
}


int tree_traversal(struct astree* root) {
   symbol_stack.push_back(new symbol_table());
   type_names = new symbol_table();

   next_block = 0;
   block_stack.push_back(next_block++);
   
   return tree_traversal_rec(root);
}
