// $Id: symbol.h,v 1.1 2017-04-09 17:04:29-07 - - $

#ifndef __SYMBOL_H__
#define __SYMBOL_H__

#include <string>
#include <vector>
#include <bitset>
#include <unordered_map>

using namespace std;

extern FILE* symFile;

// always update the string vector in astree.cpp 
// when updating this enum
enum { 
   ATTR_void,
   ATTR_int,
   ATTR_null,
   ATTR_string,
   ATTR_struct,
   ATTR_array,
   ATTR_function,
   ATTR_variable,
   ATTR_field,
   ATTR_typeid,
   ATTR_param,
   ATTR_lval,
   ATTR_const,
   ATTR_vreg,
   ATTR_vaddr,
   ATTR_prototype,
   ATTR_bitset_size
};

struct symbol;

using attr_bitset = bitset<ATTR_bitset_size>;
using symbol_table = unordered_map<const string*, symbol*>;
using symbol_entry = symbol_table::value_type;


extern vector<astree*> stringConsVector;
extern symbol_table* type_names;
extern vector<symbol_table*> symbol_stack;

typedef struct symbol {
   
   attr_bitset attributes;
   symbol_table* fields;
   location lloc;
   size_t blocknr;
   vector<symbol*>* parameters;
   const string* type_id; 
   
   symbol(astree* node, size_t _blocknr);

} symbol_t;

int tree_traversal (astree* node);
int set_basetype (symbol* sym, astree* basetype_p);
// int is_compatible(symbol* sym, astree* expr);
int insert_function(astree* node);
int find_ident(astree* node);
// void sym_dump(FILE* file);
// void print_sym(symbol* sym, int block);


#endif
